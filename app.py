from flask import Flask

UPLOAD_FOLDER = 'static/'

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['FOOD_LIST'] = ["Aaloo_Bonda", "Anarsa", "Apple", "Apricot", "Balu_Shahi", "Banana",
"Bhel_Puri", "Cauliflower", "Chikki", "Chole_Bature", "Corn", "Dabeli",
"Dahi_Puri", "Dates", "Dhokla", "Dosa", "Fafda", "Gajar_Halwa", "Ghevar",
"Ginger", "Gulab_Jamun", "Idli", "Jalebi", "Jira_Rice", "Kachori", "Kajukatli",
"KesarPeda", "Khandvi", "Kiwi", "Lemon", "Limes", "Lychee", "Meduvada",
"Motichoor_laddu", "Murukku", "Mysorepak", "Nachos", "Onion", "Orange",
"Pancakes", "Panipuri", "Papad", "Papaya", "Patra", "PavBhaji", "Pear",
"Pepper_Green", "Pepper_Orange", "Pepper_Red", "Pineapple", "Pitahaya_Red",
"Pizza", "Poha", "Pomegranate", "Ras_Malai", "Raspberry", "Raw_Potato",
"Red_velvet_cake", "Sabudana_khichadi", "Samosa", "Sohan_papdi", "Strawberry",
"Tomato", "Upma", "Vada_Pav", "Walnut", "Watermelon"]
