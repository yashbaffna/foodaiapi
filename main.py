#run as to visible same network over wifi lan---flask run --host=0.0.0.0
import os
import cv2
import numpy as np
import pandas as pd
from app import app
import urllib.request
from flask import Flask, request, redirect, url_for, render_template,jsonify
from werkzeug.utils import secure_filename

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
import tensorflow as tf


food_list = app.config['FOOD_LIST']
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
nutrition_file=(os.path.join(app.config['UPLOAD_FOLDER'])+ 'nutrition/Nutrients.xlsx')

def predict_class(model, img):
	img = image.load_img(img, target_size=(299, 299))
	img = image.img_to_array(img)                    
	img = np.expand_dims(img, axis=0)         
	img /= 255.                                      

	pred = model.predict(img)
	index = np.argmax(pred)
	food_list.sort()
	pred_value = food_list[index]
	return pred_value



def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
	
@app.route('/')
def upload_form():
	return 'Wrong page open'

@app.route('/uploadimage', methods=['POST'])
def upload_image():
	if 'file' not in request.files:
		print ('no file')
		return jsonify('No file part')
		
	file = request.files['file']
	if file.filename == '':
		print ('not selected')
		return jsonify('No image selected for uploading')
	
	if file and allowed_file(file.filename):
		filename = secure_filename(file.filename)
		file.save(os.path.join(app.config['UPLOAD_FOLDER']+'images', filename))
		#print('upload_hdff model path: ' + os.path.join(app.config['UPLOAD_FOLDER'])+'best_model_class.hdf5')
		path_model=os.path.join(app.config['UPLOAD_FOLDER'])+'model/trained_foodai_model_32_2.hdf5'

		image_path=os.path.join(app.config['UPLOAD_FOLDER'])+'images/'+filename

		#print(image_path)
		model_best = load_model(path_model,compile = False)
		food_val=predict_class(model_best, image_path)
	
		return jsonify(food_val)
		
	else:
		print ('not png')
		return jsonify('Allowed image types are -> png, jpg, jpeg')



@app.route('/nutrients/<food>')
def get_nutrients(food):

	df = pd.read_excel (nutrition_file) 
	res=df.loc[df['FoodName'] == food.lower()]
	print (res)
	result={}
	for index, row in res.iterrows():
		result['foodID']=str(row['foodID'])
		result['FoodName']=str(row['FoodName'])
		result['Serving']=str(row['Serving'])
		result['Energy']=str(row['Energy'])
		result['Protein']=str(row['Protein'])
		result['Carbohydrates']=str(row['Carbohydrates'])
		result['Fiber']=str(row['Fiber'])
		result['Fat']=str(row['Fat'])
		result['Cholestrol']=str(row['Cholestrol'])
		result['Vitamin_A']=str(row['Vitamin_A'])
		result['Vitamin_B1']=str(row['Vitamin_B1'])
		result['Vitamin_B2']=str(row['Vitamin_B2'])
		result['Vitamin_B3']=str(row['Vitamin_B3'])
		result['Vitamin_C']=str(row['Vitamin_C'])
		result['Vitamin_E']=str(row['Vitamin_E'])
		result['Vitamin_B9']=str(row['Vitamin_B9'])
		result['Calcium']=str(row['Calcium'])
		result['Iron']=str(row['Iron'])
		result['Magnesium']=str(row['Magnesium'])
		result['Phosporous']=str(row['Phosporous'])
		result['Sodium']=str(row['Sodium'])
		result['Potassium']=str(row['Potassium'])
		result['Zinc']=str(row['Zinc'])
	return jsonify(result)



if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5000)
